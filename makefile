CC=gcc
CFLAGS=-I

all: slider.c
	$(CC) -o slider slider.c

.PHONY: clean

clean:
	rm -f slider
