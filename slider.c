#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>			//Needed for I2C port
#include <fcntl.h>			//Needed for I2C port
#include <sys/ioctl.h>			//Needed for I2C port
#include <linux/i2c-dev.h>		//Needed for I2C port
#include <signal.h>			//Handle SIGINT
#include <unistd.h>			//gives sleep
#include <errno.h>			//gives errno

volatile sig_atomic_t flag = 0;
void set_close_flag(int sig){
  flag = 1;
}

int main( int argc, char *argv[] )
{
	int file_i2c;
	int length;
	unsigned char buffer[1] = {0};
	//char *filename = (char*)"/dev/i2c-1";	// The bus
	//int addr = 0x8;          		// The slave

	if ( argc != 3 )
	{
		printf("Wrong number of arguments\n");
		printf("sliders <i2c-bus-file> <slave-id-in-dec>\n"); //TODO support hex
		return 1;
	}

	// TODO Error handling
	char *filename = (char*)argv[1];
	int addr = atoi(argv[2]);

	printf("Starting \nPID : \t\t%ld\nPPID : \t\t%ld\n"
			"Bus : \t\t%s\nSlave : \t0x%x\n\n",
			(long)getpid(), getppid(), filename, addr);

	signal(SIGINT, set_close_flag); 
	signal(SIGTERM, set_close_flag); 

	// Open bus
	if ((file_i2c = open(filename, O_RDWR)) < 0)
	{
		perror("Failed to open i2c bus");
		return 1;
	}

	if (ioctl(file_i2c, I2C_SLAVE, addr) < 0)
	{
		perror("Failed to acquire access");
		int ret = close(file_i2c);
		return 1;
	}

	// Read bytes
	length = 1;			//<<< Number of bytes to read
	while(1)
	{
		if(flag)
		{
			flag=0;
			printf("\nKill signal recived\n");
			break;
		}

		//read() returns the number of bytes actually read,
		//if it doesn't match then an error occurred 
		//(e.g. no response from the device)
		if (read(file_i2c, buffer, length) != length)		 
		{
			perror("Read failed");
			break;
		}
		else
		{
			printf("%c[2K", 27);
			printf("\rData read : %d", buffer[0]);
			fflush(stdout);
		}
		sleep(1);	
	}

	printf("\nClosing...\n");
	if(close(file_i2c) != 0)
	{
		perror("Failed closing bus");
	}
	return 0;
}
